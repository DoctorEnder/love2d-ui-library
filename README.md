# UI Library

More than just a library, it's a structure to make quick games/programs without setting up everything up from scratch every time.



# How to use

Just copy everything, add your own config file, and start adding folders and assest as you please.


# States

States are simply the file that is being treated as the new main code loop.

This allows for easy seperation of things like menus and the game itself. You could even pack in multiple diferent games.

The default state is Splash.lua because who doesen't want that.


Here's some important stuff.
```
#!lua

--located in main.lua--
Game.setup(
    {
        Width = 1280, Height = 720,--These are hard coded and do not change after you set them
        
        Source = "", --This is where the file/folder search will start from 
        
        State = "Splash.lua", --The first state that your game will open with.   
    }
)

```

Call Game.setState whenever you want to switch to a diferent state.

Game.setState("empty.lua") -- Pass in the name of the lua file with the state code

