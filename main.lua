
local loadLibs = function()
	local files = love.filesystem.getDirectoryItems("src/Libs")
	for _,f in ipairs(files) do
		local ch = love.filesystem.load("src/Libs/"..f)
		if ch then
			ch()
		end
	end
end loadLibs()

function love.load()
	math.randomseed(os.time())
	love.window.setIcon(love.image.newImageData(UI.searchForFile("","Your Icon.png")))
	Game.setup(
		{
			Width = 1280,
			Height = 720,
			Source = "",
			State = "Splash.lua",
		}
	)
end

function love.update(dt)
	Game.update(dt)
end

function love.draw()
	Game.draw()
end
